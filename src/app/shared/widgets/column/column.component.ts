import { Component, OnInit, AfterContentInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-widget-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.sass']
})
export class ColumnComponent implements OnInit, AfterContentInit {

  Highcharts = Highcharts;
  barOptions: {};
  missReadAge: {}[];
  baseUrl = 'https://smartmeteranalytics.uc.r.appspot.com';
  categories: any = [];
  values: string[];
  isLoaded = false;
  // optionsLoaded: Promise<boolean>;

  @Input() data = [];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    // let result;
    // Landing Page - miss reads aging
    // slug: /apis/landingpage/miss_reads_aging
    const missReadsAgingApi = '/apis/landingpage/miss_reads_aging';
    // fetch(this.baseUrl + missReadsAgingApi).then((resp) => resp.json())
    //   .then(data => {
    //     this.missReadAge = data as any;
    //     this.categories = Object.keys(this.missReadAge[0]);
    //     this.values = Object.values(this.missReadAge[0]);
    //     // this.values = result.map(e => e.toString());
    //     console.log(this.missReadAge, this.categories, this.values);
    //     // this.isLoaded = true;
    //     this.optionsLoaded = Promise.resolve(true);
    //   });

    this.httpClient.get(this.baseUrl + missReadsAgingApi).subscribe(data => {
      this.missReadAge = data as any;
      this.categories = Object.keys(this.missReadAge[0]);
      this.values = Object.values(this.missReadAge[0]);
      // this.values = result.map(e => e.toString());
      console.log(this.missReadAge, this.categories, this.values);
      // this.isLoaded = true;
      // this.optionsLoaded = Promise.resolve(true);
      // Highcharts.chart('container', this.barOptions);
    });

    const cat = ['1-6 Days','7-13 Days','14-20 Days','21-29 Days','30+ Days'];
    this.barOptions =
    {
      chart: {
        renderTo: 'container',
        type: 'bar',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 340,
          depth: 72,
          viewDistance: 25
        }
      },
      title: {
        text: 'Missed Read Meter Count- Aging'
      },
      subtitle: {
        enabled: false
      },
      xAxis: {
        // categories: this.categories,
        // categories: ['1-6 Days','7-13 Days','14-20 Days','21-29 Days','30+ Days'],
        categories: cat,
        title: {
          text: 'Days'
        }
      },
      credits: {
        enabled: false
      },
      yAxis: {
        title: {
          text: 'Missed Read Meter',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      series: [{
        name: 'Aging',
        // data: this.values,
        data: [1, 2, 3, 4, 5]
      }]
    },
    Highcharts.setOptions(this.barOptions);
    // HC_exporting(this.Highcharts);
  }

  ngAfterContentInit(): void {
    // Called after ngOnInit when the component's or directive's content has been initialized.
    // Add 'implements AfterContentInit' to the class.
    // Highcharts.setOptions(this.barOptions);
  }

}
