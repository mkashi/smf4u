import { Component, OnInit } from '@angular/core';
// import { DashboardService } from '../dashboard.service';
import { HttpClient} from '@angular/common/http';
// import { FlxUiDatatableModule, FlxUiDataTable } from 'flx-ui-datatable';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  bigChart = [];
  line = [];
  baseUrl = 'https://smartmeteranalytics.uc.r.appspot.com';

  // constructor(private dashboardService: DashboardService) { }
  serverData: JSON;
  columns: string[];
  rows: {}[];
  meterCount: any = {};
  irRate: any = {};
  isLoaded = false;

  constructor(private httpClient: HttpClient) {}

  getData() {
    this.httpClient.get('http://127.0.0.1:5002/stocks').subscribe(data => {
      this.serverData = data as JSON;
      console.log(this.serverData);
    });
  }

  convert(str) {
    const date = new Date(str);
    const mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-');
  }

  ngOnInit(): void {
    // this.bigChart = this.dashboardService.bigChart();
    // this.line = this.dashboardService.line();
    let result;
    // this.httpClient.get('http://127.0.0.1:5002/stocks').subscribe(data => {
    //   result = data as JSON;
    //   console.log(result);
    //   this.columns = Object.keys(result[0]);
    //   this.rows = result;
    // });

    // Landing Page - miss reads meter customer cnt
    // slug: /apis/landingpage/mc_cnt
    const mcCntApi = '/apis/landingpage/mc_cnt';
    this.httpClient.get(this.baseUrl + mcCntApi).subscribe(data => {
      this.meterCount = data[0] as JSON;
      console.log(this.meterCount);
    });

    // Landing Page - miss reads interval register rate
    // slug:	/apis/landingpage/ir_rate
    const irApi = '/apis/landingpage/ir_rate';
    this.httpClient.get(this.baseUrl + irApi).subscribe(data => {
      this.irRate = data[0] as JSON;
      console.log(this.irRate);
    });

    // Landing Page - miss reads details
    // slug: /apis/landingpage/miss_reads_detail
    const missReadDetailApi = '/apis/landingpage/miss_reads_detail';
    this.httpClient.get(this.baseUrl + missReadDetailApi).subscribe(data => {
      result = data as JSON;
      this.columns = Object.keys(result[0]);
      this.rows = result;
      console.log(result);
    });

    this.isLoaded = true;


  }
  LinktoCity(order: string) {
    window.open(order, '_blank');
  }
}
