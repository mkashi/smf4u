import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-subview',
  templateUrl: './subview.component.html',
  styleUrls: ['./subview.component.sass']
})
export class SubviewComponent implements OnInit {

  mdrSummary: {}[];
  mdrDetail: {}[];
  columns: string[];
  rows: {}[];
  baseUrl = 'https://smartmeteranalytics.uc.r.appspot.com';
  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    // Master data reconiciliation Summary
    // slug: /apis/mdr/summary

    const mdrSummaryApi = '/apis/mdr/summary';
    this.httpClient.get(this.baseUrl + mdrSummaryApi).subscribe(data => {
      this.mdrSummary = data as any;
      // this.categories = Object.keys(this.missReadAge[0]);
      // this.values = Object.values(this.missReadAge[0]);
      // this.values = result.map(e => e.toString());
      console.log(this.mdrSummary);
      // this.isLoaded = true;
      // this.optionsLoaded = Promise.resolve(true);
      // Highcharts.chart('container', this.barOptions);
    });

    // Master data reconiciliation Detail
    // slug: /apis/mdr/detail

    const mdrDetailApi = '/apis/mdr/detail';
    this.httpClient.get(this.baseUrl + mdrDetailApi).subscribe(data => {
      this.mdrDetail = data as any;
      // this.categories = Object.keys(this.missReadAge[0]);
      // this.values = Object.values(this.missReadAge[0]);
      // this.values = result.map(e => e.toString());
      this.columns = Object.keys(this.mdrDetail[0]);
      this.rows = this.mdrDetail;
      console.log(this.mdrDetail);
      // this.isLoaded = true;
      // this.optionsLoaded = Promise.resolve(true);
      // Highcharts.chart('container', this.barOptions);
    });
  }

}
