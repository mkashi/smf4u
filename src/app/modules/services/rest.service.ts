import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class RestService {
    serverData: JSON;
    employeeData: JSON;
    // weatherUrl: string = "http://127.0.0.1:5002/stocks";

    constructor(private httpClient: HttpClient) { }

    // constructor() { }
    getStocks() {
        this.httpClient.get('http://127.0.0.1:5002/stocks').subscribe(data => {
            this.serverData = data as JSON;
            console.log(this.serverData);
        });
    }
}
