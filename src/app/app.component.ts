import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Dashboard';

  serverData: JSON;

  constructor(private httpClinet: HttpClient) {}

  getData() {
    this.httpClinet.get('http://127.0.0.1:5002/stocks').subscribe(data => {
      this.serverData = data as JSON;
      console.log(this.serverData);
    });
  }
}
