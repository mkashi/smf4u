import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultModule } from './layouts/default/default.module';
// import { FlxUiDatatableModule, FlxUiDataTable } from 'flx-ui-datatable';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // FlxUiDatatableModule,
    AppRoutingModule,
    DefaultModule
  ],
  providers: [
    // FlxUiDataTable
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
